<h2>Malware Detection based on Network Traffic using Reinforcement Learning</h2>
  <br>
  <p>We classify the network traffic as malicious or benign using Reinforcement
learning algorithm (DQN) and compare its result with other supervised
learning classifiers such as Logistic Regression, Neural Networks, Decision
    trees classifier</p>
