# import
from __future__ import division, print_function, absolute_import
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.impute import SimpleImputer
import numpy as np
import os
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
# init
if True:
    # mapMainLabel :  {0: 67343, 1: 45927, 2: 52, 3: 995, 4: 11656}
    # size of dataTrain:  (125973, 41)
    # size of dataTest:  (18794, 41)
    # size of dataOutlier:  (3750, 41)
    # folder = '../../data/' # ggcolab
    fileDir = os.path.dirname(os.path.realpath(__file__))
    folder = '' # desktop
    dataTrainPath = os.path.join(fileDir, folder + 'NSL-KDD/csv_result-KDDTrain+_20Percent.csv')
    dataTestPath = os.path.join(fileDir, folder + 'NSL-KDD/csv_result-KDDTest+.csv')
    feature_name = ['duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment',
                    'urgent', 'hot', 'num_failed_logins', 'logged_in', 'num_compromised', 'root_shell', 'su_attempted',
                    'num_root', 'num_file_creations', 'num_shells', 'num_access_files', 'num_outbound_cmds', 'is_host_login',
                    'is_guest_login', 'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate', 'srv_rerror_rate',
                    'same_srv_rate', 'diff_srv_rate', 'srv_diff_host_rate', 'dst_host_count', 'dst_host_srv_count',
                    'dst_host_same_srv_rate', 'dst_host_diff_srv_rate', 'dst_host_same_src_port_rate', 'dst_host_srv_diff_host_rate',
                    'dst_host_serror_rate', 'dst_host_srv_serror_rate', 'dst_host_rerror_rate', 'dst_host_srv_rerror_rate']
    string_columns = ['protocol_type', 'service', 'flag']
    label_name = 'class'
    old_label = ['normal', 'anomaly', 'back', 'buffer_overflow', 'ftp_write', 'guess_passwd' ,'imap' ,'ipsweep' ,'land' ,'loadmodule' ,
                 'multihop' ,'neptune' ,'nmap', 'perl' ,'phf', 'pod' ,'portsweep' ,'rootkit', 'satan' ,'smurf'	,'spy', 'teardrop' ,'warezclient', 'warezmaster']
    new_label = ['normal', 'anomaly', 'dos' ,'u2r', 'r2l', 'r2l',	'r2l',	'probe',	'dos',	'u2r',	'r2l',	'dos',	'probe'	,'u2r',
                 'r2l',	'dos',	'probe'	,'u2r',	'probe',	'dos',	'r2l',	'dos',	'r2l',	'r2l']
    mapLabel = {}
    for i in range(len(old_label)):
        mapLabel[old_label[i]] = new_label[i]
    mapLabel2Number = {'normal': 0, 'dos': 1, 'u2r': 2, 'r2l': 3 , 'probe': 4, 'anomaly':1}
    # main_label = ['back','guess_passwd','ipsweep','neptune','nmap','normal','pod','portsweep','satan','smurf','warezmaster']
    # mapMainLabel = {}
    # for i in range(len(main_label)):
    #     mapMainLabel[main_label[i]] = i
    n_dimension = 41
# store function
if True:
    def normalLabel(labelTrain, labelTest):
        templabelTrain = []
        templabelTest = []
        for i in range(len(labelTrain)):
            aLabel = labelTrain[i][0]
            lenStr = len(aLabel)
            if aLabel[lenStr-1] == '.':
                aLabel = aLabel[0:lenStr-1]

            templabelTrain.append(aLabel)

        for i in range(len(labelTest)):
            templabelTest.append(labelTest[i][0])

        # encoder for label
        le = preprocessing.LabelEncoder()
        le.fit(templabelTrain)
        print(le.classes_)

        le.fit(templabelTest)
        print(le.classes_)

        return templabelTrain, templabelTest

    def filterMainLabel(dataTrain, dataTest, labelTrain, labelTest):
        labelArray = []
        mapMainLabel = {}
        mapMainLabelTest = {}

        le = preprocessing.LabelEncoder()
        le.fit(labelTrain)
        # print(le.classes_)

        # init value for countLabel
        labelArray = le.classes_
        for i in range(len(labelArray)):
            mapMainLabel[labelArray[i]] = 0
            mapMainLabelTest[labelArray[i]] = 0

        # count
        for i in range(len(labelTrain)):
            mapMainLabel[labelTrain[i]] = mapMainLabel[labelTrain[i]] + 1
        for i in range(len(labelTest)):
            mapMainLabelTest[labelTest[i]] = mapMainLabelTest[labelTest[i]] + 1
        print ("mapMainLabel : ", mapMainLabel)
        print ("mapMainLabelTest : ", mapMainLabelTest)



        #  start filtering
        tempDataTrain = []
        tempDataTest = []
        tempLabelTrain = []
        tempLabelTest = []

        for i in range(len(dataTrain)):
            # if labelTrain[i] in mapMainLabel.keys():
            if mapMainLabel[labelTrain[i]] > 0:
                tempLabelTrain.append(labelTrain[i])

                tempData = []
                # todo: error n_dimension. Change n_dimension - 1 to n_dimension
                for j in range(n_dimension):
                    tempData.append(dataTrain[i][j])
                tempDataTrain.append(tempData)

        for i in range(len(dataTest)):
            # if labelTest[i] in mapMainLabel.keys():
            if mapMainLabel[labelTest[i]] > 0:
                tempLabelTest.append(labelTest[i])

                tempData = []
                for j in range(n_dimension):
                    tempData.append(dataTest[i][j])
                tempDataTest.append(tempData)

        return  np.array(tempDataTrain), np.array(tempDataTest), np.array(tempLabelTrain), np.array(tempLabelTest)

    def changeLabel2FourLabel(labelTrain, labelTest):
        templabelTrain = []
        templabelTest = []
        for i in range(len(labelTrain)):
            newLabel = mapLabel[labelTrain[i]]
            templabelTrain.append(newLabel)
        for i in range(len(labelTest)):
            newLabel = mapLabel[labelTest[i]]
            templabelTest.append(newLabel)


        return templabelTrain, templabelTest

    def changeLabel2FourLabel_number(labelTrain, labelTest):
        templabelTrain = []
        templabelTest = []
        for i in range(len(labelTrain)):
            newLabel = mapLabel2Number[labelTrain[i]]
            templabelTrain.append(newLabel)
        for i in range(len(labelTest)):
            newLabel = mapLabel2Number[labelTest[i]]
            templabelTest.append(newLabel)


        return templabelTrain, templabelTest

    def encoderDataIsLabel(dataTrain, dataTest, dataOutlier):
        lenTrain = len(dataTrain)
        lenTest = len(dataTest)
        lenOutlier = len(dataOutlier)

        for j in range(1,4): # get parameter is label
    ##########################################################################
            # encoder label train
            colDataTrain = []
            for i in range(lenTrain):
                colDataTrain.append(dataTrain[i][j])

            le = preprocessing.LabelEncoder()
            le.fit(colDataTrain)

            tranformTrain = le.transform(colDataTrain)
            for i in range(lenTrain):
                dataTrain[i][j] = tranformTrain[i]
    ##########################################################################
            # encoder label test
            colDataTest = []
            for i in range(lenTest):
                colDataTest.append(dataTest[i][j])

            # todo: thêm
            le.fit(colDataTest)

            tranformTest = le.transform(colDataTest)
            for i in range(lenTest):
                dataTest[i][j] = tranformTest[i]

    ##########################################################################
            # encoder label outlier
            colDataOutlier = []
            for i in range(lenOutlier):
                colDataOutlier.append(dataOutlier[i][j])

            tranformOutlier = le.transform(colDataOutlier)
            for i in range(lenOutlier):
                dataOutlier[i][j] = tranformOutlier[i]


        dataTrain = np.array(dataTrain)
        dataTest = np.array(dataTest)
        dataOutlier = np.array(dataOutlier)

        return dataTrain, dataTest, dataOutlier
    # TODO: change numLabel = 5 to 2
    def oneHostEncodingLabel(labelTrain, labelTest, numLabel = 2):
        tempTrain = []
        tempTest = []
        for num in labelTrain:
            arr = np.zeros(numLabel)
            arr[num] = 1
            tempTrain.append(arr)
        for num in labelTest:
            arr = np.zeros(numLabel)
            arr[num] = 1
            tempTest.append(arr)

        return np.array(tempTrain), np.array(tempTest)

    def normalData(dataTrain, dataTest, dataOutlier):
        scaler = MinMaxScaler()
        scaler.fit(dataTrain)
        dataTrain = scaler.transform(dataTrain)
        dataTest = scaler.transform(dataTest)
        # todo: comment dataOutlier vì chưa dùng đến
        # dataOutlier = scaler.transform(dataOutlier)

        return np.array(dataTrain),  np.array(dataTest),  np.array(dataOutlier)

    def find_name(array, value):
        if (len(array) <=0):
            return  -1
        for i in range(len(array)):
            if value == array[i]:
                return i
        return -1

    def filterData(dataTest, labelTrain, labelTest):
        tempDataTest = []
        tempLabelTest = []

        outlierData = []
        outlierLabel = []

        # reduce labelTrain
        le = preprocessing.LabelEncoder()
        le.fit(labelTrain)
        reduceLabelTrain = le.classes_
        # print (reduceLabelTrain)

        for i in range(len(dataTest)):
            alabelTest = labelTest[i]
            index = find_name(reduceLabelTrain, alabelTest)

            tempArray = []
            # todo: change n_dimension - 1 to n_dimension
            for j in range(n_dimension):# loc truong id va label
                tempArray.append(dataTest[i][j])

            if index > -1: #loc data co nhan ko nam trong tap train
                # print (alabelTest)
                tempLabelTest.append(alabelTest)
                tempDataTest.append(tempArray)
            else:
                outlierLabel.append(alabelTest)
                outlierData.append(tempArray)

        return tempDataTest, tempLabelTest, outlierData, outlierLabel
# using function
if True:
    def saveEncoder(data):
        for i in string_columns:
            label_encoder = LabelEncoder()
            data[i] = label_encoder.fit_transform(data[i])
            np.save(str(i+'.npy'), label_encoder.classes_, allow_pickle=True)
        return data
    """
        label_encoder_dict = {}
    label_encoder = LabelEncoder()
            data[i] = label_encoder.fit_transform(data[i])
                    if data_encoded is None:
                data_encoded = temp
            else:
                data_encoded = np.concatenate((data, temp), axis=1)
        onehotencoder_dict = {}
        onehot_encoder = OneHotEncoder(sparse=False)
        temp = onehot_encoder.fit_transform(data)
        onehotencoder_dict[i] = onehot_encoder
    """
    def saveEncoder_label(data):
        label_encoder = LabelEncoder()
        temp = label_encoder.fit_transform(data)
        np.save(str(label_name+'.npy'), label_encoder.classes_, allow_pickle=True)
        return temp
    def loadEncoded(data):
        for i in string_columns:
            encoder = LabelEncoder()
            encoder.classes_ = np.load(str(i + '.npy'), allow_pickle=True)
            data[i] = encoder.transform([data[i]])[0]
        return data
    """
        onehot_encoder = onehotencoder_dict[i]
        temp = onehot_encoder.transform(data)
    """
    def loadEncoded_label(data):
        encoder = LabelEncoder()
        encoder.classes_ = np.load(str(label_name + '.npy'), allow_pickle=True)
        data = encoder.transform([data])[0]
        return data
    def readData(filePath):
        df_Data = pd.read_csv(filePath)
        for column_name in feature_name:
            if column_name not in string_columns:
                df_Data[column_name] = df_Data[column_name].map(float)
        feature = df_Data.iloc[:, 1:-1]
        label = df_Data.iloc[:, -1]
        return feature, label
    """
            feature = df_Data.iloc[:, 1:-1].values
        label = df_Data.iloc[:, -1].values
                n_dimension = df_Data.shape[1] - 2
                count = 1
                if count == 0:
                    break
                else:
                    count -= 1
                print(type(df_Data[column_name][0]))
                df_Data[column_name] = df_Data[column_name].map(int)
                for i in feature_name:
                    print(type(df_Data[i][0]))
                df_Data = df_Data.as_matrix()
                dataset = df_Data.values
                X_train = df_Data[:,0:37]
                data = df_Data[:, 4:41]
                data = df_Data[:, 0:n_dimension]
                label = df_Data[:,41:42]
                print(type(a[0]))
    """
    def display(feature, label):
        print('feature: \n',feature, 'label: \n',label)
    def getData(train = True):
        if train == True:
            feature, label = readData(dataTrainPath)
        else:
            feature, label = readData(dataTestPath)
        # display(feature, label)
        feature = saveEncoder(feature)
        # label = saveEncoder_label(label)
        # display(feature, label)
        return feature, label
    def get_feature_name():
        return feature_name
if __name__=="__main__":
    feature, label = readData(dataTrainPath)
    display(feature, label)
    saveEncoder(feature)
    saveEncoder_label(label)
    display(feature, label)

    # unuse main
    """
    test_feature, test_label = readData(dataTestPath)
    a_label = test_label[1]
    print('a_feature:\n', a_label)
    a_label = loadEncoded_label(a_label)
    print('a_feature:\n', a_label)
        saveEncoder(feature)
    test_feature, test_label = readData(dataTrainPath)
    a_feature = test_feature.iloc[1,:]
    print('a_feature:\n', a_feature)
    a_feature = loadEncoded(a_feature)
    print('a_feature:\n', a_feature)
encoder.classes_ = np.load('protocol_type.npy', allow_pickle=True)
a_feature = feature.iloc[1, :]
    print('a_feature:\n', a_feature)
    a_feature['protocol_type'] = encoder.transform([a_feature['protocol_type']])[0]
    print('a_feature: ', a_feature)
    feature = setEncoder(feature)
    feature, label = readData(dataTrainPath)
    print(a_feature)
    display(feature, label)
    """