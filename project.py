# -*- coding: utf-8 -*-

import time
import copy
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.nn.functional as F
from sklearn import metrics

"""Loading Dataset"""

df = pd.read_csv('~/PycharmProjects/MDU/malware-detection-using-RL/dataset/data.csv')
df.head()

df['Label'].value_counts()

"""Train and Test split"""

from sklearn.model_selection import train_test_split

feature_cols = ['Packets', 'Bytes','Packets_sent','Bytes_sent','Packets_received','Bytes_received','Bits_sent_rate','Bits_received_rate']
X = df[feature_cols] #Features
y = df.Label #target

X.describe()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 1)

"""Data Preprocessing"""

from sklearn.preprocessing import normalize 

#Normal
X_train = normalize(X_train, norm = 'l1')
X_test = normalize(X_test, norm = 'l1')

y_train = y_train.map({'Benign': 0, 'Malicious': 1})
y_test = y_test.map({'Benign': 0, 'Malicious': 1})

X_train

y_test.value_counts()

"""Using D-Tree Classifier"""

from sklearn.tree import DecisionTreeClassifier
from sklearn import metrics

clf = DecisionTreeClassifier()

clf = clf.fit(X_train,y_train)

y_pred = clf.predict(X_test)

print("Accuracy:",metrics.accuracy_score(y_test, y_pred))

"""Using Logistic Regression"""

from sklearn.linear_model import LogisticRegression

logreg = LogisticRegression()
logreg.fit(X_train,y_train)
y_pred_lr = logreg.predict(X_test)

print("Accuracy:",metrics.accuracy_score(y_test, y_pred_lr))

"""Using Neural Networks"""

import torch
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
  def __init__(self):
    super(Net,self).__init__()

    self.fc1 = nn.Linear(8,30)
    self.fc2 = nn.Linear(30,2)

  def forward(self,x):
    x = self.fc1(x)
    x = torch.tanh(x)
    x = self.fc2(x)
    return x

  def predict(self,x):
    pred = F.softmax(self.forward(x),dim=1)
    ans = []
    for t in pred:
      if t[0]>t[1]:
        ans.append(0)
      else:
        ans.append(1)
    return torch.tensor(ans)

model = Net()
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

train_X = torch.Tensor(X_train)
train_y = torch.from_numpy(y_train.values)
test_X = torch.Tensor(X_test)
test_y = torch.from_numpy(y_test.values)

epochs = 500
losses = []
for i in range(epochs):
  y_pred = model.forward(train_X.float())
  loss = criterion(y_pred,train_y)
  losses.append(loss.item())
  optimizer.zero_grad()
  loss.backward()
  optimizer.step()

from sklearn.metrics import accuracy_score
print(accuracy_score(model.predict(test_X.float()),test_y))

"""Using Reinforcement Learning - Q Network"""

column_names = ['Packets', 'Bytes','Packets_sent','Bytes_sent','Packets_received','Bytes_received','Bits_sent_rate','Bits_received_rate']

X_train = pd.DataFrame(X_train, columns = column_names)
X_test = pd.DataFrame(X_test, columns = column_names)
y_train = pd.DataFrame(y_train)
y_test = pd.DataFrame(y_test)

X_train

"""MDP Environment"""


class Environment1:

    def __init__(self, data, label, history_t=8):
        self.label = label
        self.data = data
        self.history_t = history_t
        self.reset()

    def reset(self):
        self.t = 0
        self.done = False
        self.profits = 0
        self.position = []
        self.history = []
        self.history = [0 for _ in range(self.history_t)]
        return self.history  # obs

    def step(self, act):
        reward = abs(act - self.label.iloc[self.t, :]['Label'])
        if reward > 0:
            reward = -1
        else:
            reward = 1

        self.t += 1

        self.history = []
        self.history.append(self.data.iloc[self.t, :]['Packets'])
        self.history.append(self.data.iloc[self.t, :]['Bytes'])
        self.history.append(self.data.iloc[self.t, :]['Packets_sent'])
        self.history.append(self.data.iloc[self.t, :]['Bytes_sent'])
        self.history.append(self.data.iloc[self.t, :]['Packets_received'])
        self.history.append(self.data.iloc[self.t, :]['Bytes_received'])
        self.history.append(self.data.iloc[self.t, :]['Bits_sent_rate'])
        self.history.append(self.data.iloc[self.t, :]['Bits_received_rate'])

        if (self.t == len(self.data) - 1):
            self.done = True

        return self.history, reward, self.done


env = Environment1(X_train, y_train)

"""Deep Q Network"""

import torch
import torch.nn as nn
import torch.nn.functional as F

class Q_Network(nn.Module):
  def __init__(self, obs_len, hidden_size, actions_n):
    super(Q_Network,self).__init__()

    self.fc_val = nn.Sequential(
        nn.Linear(obs_len, hidden_size),
        nn.ReLU(),
        nn.Linear(hidden_size, actions_n),
        nn.Softmax(dim=1)
    )

  def forward(self, x):
    h = self.fc_val(x)
    print(x.shape)
    return (h)

input_size = 8
output_size = 2
hidden_size = 50
USE_CUDA = False
LR = 0.001

Q = Q_Network(input_size, hidden_size, output_size)

Q_ast = copy.deepcopy(Q)

if USE_CUDA:
    Q = Q.cuda()
loss_function = nn.MSELoss()

#defineing the optimizer
optimizer = optim.Adam(list(Q.parameters()), lr=LR)

"""Deep Q Learning"""

epoch_num = 10
step_max = len(env.data)
memory_size = 32
batch_size = 8
gamma = 0.97

memory = []  #Replay Memory
total_step = 0
total_rewards = []
total_losses = []
epsilon = 1.0  #exploration rate
epsilon_decrease = 1e-3
epsilon_min = 0.1
start_reduce_epsilon = 200
train_freq = 10
update_q_freq = 20
gamma = 0.97  #discount rate
show_log_freq = 5

accuracy_per_epoch = []

def testing():

  test_env = Environment1(X_test,y_test)
  pobs = test_env.reset()
  test_acts = []
  test_rewards = []
  current_cash_in_hand = []

  for _ in range(len(test_env.data)-1):
    
      pact = Q(torch.from_numpy(np.array(pobs, dtype=np.float32).reshape(1, -1)))
      pact = np.argmax(pact.data)
      test_acts.append(pact.item())
            
      obs, reward, done = test_env.step(pact.numpy())
      test_rewards.append(reward)

      pobs = obs
    
  test_acts.append(0)

  accuracy_per_epoch.append(metrics.accuracy_score(y_test,test_acts))

print(accuracy_per_epoch)

start = time.time()
for epoch in range(epoch_num):
    print(epoch+1)
    pobs = env.reset()
    step = 0
    done = False
    total_reward = 0
    total_loss = 0

    while not done and step < step_max:

        # select act using exploration
        pact = np.random.randint(2)

        # select act using exploitation
        if np.random.rand() > epsilon:
            pact = Q(torch.from_numpy(np.array(pobs, dtype=np.float32).reshape(1, -1)))
            pact = np.argmax(pact.data)
            pact = pact.numpy()

        # act
        obs, reward, done = env.step(pact)

        # add memory
        memory.append((pobs, pact, reward, obs, done))
        if len(memory) > memory_size:
            memory.pop(0)
        
        # train or update q
        if len(memory) == memory_size:
            if total_step % train_freq == 0:
                shuffled_memory = np.random.permutation(memory)   #taking random samples in order to break the correlation between consecutive samples
                memory_idx = range(len(shuffled_memory))
                for i in memory_idx[::batch_size]:
                    batch = np.array(shuffled_memory[i:i+batch_size])
                    b_pobs = np.array(batch[:, 0].tolist(), dtype=np.float32).reshape(batch_size, -1)
                    b_pact = np.array(batch[:, 1].tolist(), dtype=np.int32)
                    b_reward = np.array(batch[:, 2].tolist(), dtype=np.int32)
                    b_obs = np.array(batch[:, 3].tolist(), dtype=np.float32).reshape(batch_size, -1)
                    b_done = np.array(batch[:, 4].tolist(), dtype=np.bool)

                    q = Q(torch.from_numpy(b_pobs))
                    q_ = Q_ast(torch.from_numpy(b_obs))
                    maxq = np.max(q_.data.numpy(),axis=1)
                    target = copy.deepcopy(q.data)
                    for j in range(batch_size):
                        target[j, b_pact[j]] = b_reward[j]+gamma*maxq[j]*(not b_done[j])   #Bellman equation
                    Q.zero_grad() #clear the previous gradients
                    loss = loss_function(q, target) #compute loss
                    total_loss += loss.data.item()
                    loss.backward() #compute gradients
                    optimizer.step()  #adjust weights

            if total_step % update_q_freq == 0:
                Q_ast = copy.deepcopy(Q)
                
            # update epsilon
            if epsilon > epsilon_min and total_step > start_reduce_epsilon:
                epsilon -= epsilon_decrease

            # next step
            total_reward += reward
            pobs = obs
            step += 1
            total_step += 1

        total_rewards.append(total_reward)
        total_losses.append(total_loss)

        if (epoch+1) % show_log_freq == 0:
            log_reward = sum(total_rewards[((epoch+1)-show_log_freq):])/show_log_freq
            log_loss = sum(total_losses[((epoch+1)-show_log_freq):])/show_log_freq
            elapsed_time = time.time()-start
            print('\t'.join(map(str, ['epoch: ', epoch+1, 'epsilon: ', epsilon, 'total_step: ', total_step, 'log_reward: ', log_reward, 'log_loss: ', log_loss, 'elapsed_time: ', elapsed_time])))
            start = time.time()
    
    testing()
#return Q, total_losses, total_rewards

print(accuracy_per_epoch[47])

import matplotlib.pyplot as plt 

x = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60]
y = accuracy_per_epoch

plt.plot(x, y, color='green', linewidth = 3)
plt.xlim(1,60)
plt.xlabel('Number of epochs')
plt.ylabel('Accuracy Score')
plt.grid(True)

def annot_max(x,y, ax=None):
    xmax = x[np.argmax(y)]
    ymax = max(y)
    text= "x={:}, y={:.3f}".format(xmax, ymax)
    if not ax:
        ax=plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data',textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    ax.annotate(text, xy=(xmax, ymax), xytext=(0.94,0.96), **kw)

annot_max(x,y)

plt.savefig('accuracy_graph(1).png')